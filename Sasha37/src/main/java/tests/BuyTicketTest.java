package tests;


import bo.Account;
import bo.Passenger;
import bo.TrainRoute;
import builder.AccountBuilder;
import builder.PassengerBuilder;
import builder.TrainRouteBuilder;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.PaymentPage;

public class BuyTicketTest {

    @Test(description = "Buy train ticket test")
    public void buyTicketTrain() {
        Account account = AccountBuilder.createAccount();
        TrainRoute trainRoute = TrainRouteBuilder.createTrainRoute();
        Passenger passenger = PassengerBuilder.createPassenger();
        PaymentPage paymentPage = new HomePage().open().openLoginPage().loginByAccount(account).openBuyTicketPage().confirmationRegulations().selectTrainRoute(trainRoute).selectDate().selectTravelTime().selectTrainCar().sendDataByPassenger(passenger).selectPlaceInTrainCar().continueBuyTicket().clickPaymentPage();
        Assert.assertTrue(paymentPage.isDisplayedPaymentPage(), "Payment page not find!");
    }
}
