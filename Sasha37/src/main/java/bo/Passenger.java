package bo;


public class Passenger {

    String Lastname;
    String Name;
    String Passport;

    public Passenger (String lastname, String name, String passport) {
        Lastname = lastname;
        Name = name;
        Passport = passport;
    }

    public String getLastname() {
        return Lastname;
    }

    public String getName() {
        return Name;
    }

    public String getPassport() {
        return Passport;
    }
}
