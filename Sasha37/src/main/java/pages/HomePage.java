package pages;


import org.openqa.selenium.By;

public class HomePage extends AbstractPage {

    private static final By LOGIN_IN_SYSTEM_BUTTON_LOCATOR = By.cssSelector(".status>nobr>a>b");
    private static final String BAZE_URL = "https://poezd.rw.by";

    public HomePage open() {
        browser.open(BAZE_URL);
        return this;
    }

    public LoginPage openLoginPage() {
        if (browser.read(LOGIN_IN_SYSTEM_BUTTON_LOCATOR).equals("Вход в систему")) {
            browser.click(LOGIN_IN_SYSTEM_BUTTON_LOCATOR);
        }
        return new LoginPage();
    }
}
