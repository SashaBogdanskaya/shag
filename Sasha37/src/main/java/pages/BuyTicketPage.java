package pages;

import bo.DateDeparture;
import bo.Passenger;
import bo.TrainRoute;
import builder.DateBuilder;
import org.openqa.selenium.By;

import java.util.Calendar;


public class BuyTicketPage extends AbstractPage {

    private static final By CONFIRMATION_CHECKBOX_LOCATOR = By.xpath("//input[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form1:conf']");
    private static final By STATION_DEPARTURE_LOCATOR = By.xpath("//input[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form1:textDepStat']");
    private static final By STATION_ARRIVAL_LOCATOR = By.xpath("//input[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form1:textArrStat']");
    private static final By CALENDAR_LOCATOR = By.xpath("//img[@class='ui-datepicker-trigger']");
    private static final By CONTINUE_BUTTON_LOCATOR = By.xpath("//input[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form1:buttonSearch']");
    private static final By TRAIN_CAR_LOCATOR = By.xpath(".//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form1:tableEx1:1:_id208:0:button1']");
    private static final By PLACE_LOCATOR = By.xpath("//div[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:pass:id1:modelDiv']/div[@class='seat'][1]/a");
    private static final By LASTNAME_INPUT_LOCATOR = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:pass:tableEx1:0:lastname']");
    private static final By NAME_INPUT_LOCATOR = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:pass:tableEx1:0:name']");
    private static final By PASSPORT_INPUT_LOCATOR = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:pass:tableEx1:0:docNum']");
    private static final By YES_CONDITION_CHECKBOX = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:pass:conf']");
    private static final By CONTINUE_BUY_BUTTON = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:confirm:_id131']");
    private static final By BUY_TICKET_BUTTON = By.xpath("//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:confirm:_id182']");
    private static final By YES_BUTTON = By.xpath("//*[@value='Да']");

    private DateDeparture dateDeparture = DateBuilder.createDate();
    private String dateDepartureLocator = "//td[@data-year='" + dateDeparture.getDepartureDate().get(Calendar.YEAR) + "' and @data-month='" + dateDeparture.getDepartureDate().get(Calendar.MONTH) + "']/a[contains(text()," + dateDeparture.getDepartureDate().get(Calendar.DAY_OF_MONTH) + ")]";


    public BuyTicketPage confirmationRegulations() {
        browser.click(CONFIRMATION_CHECKBOX_LOCATOR);
        return this;
    }

    public BuyTicketPage selectTrainRoute(TrainRoute trainRoute) {
        browser.type(STATION_DEPARTURE_LOCATOR, trainRoute.getStationDeparture());
        browser.type(STATION_ARRIVAL_LOCATOR, trainRoute.getStationArrival());
        return this;
    }

    public BuyTicketPage selectDate() {
        browser.click(CALENDAR_LOCATOR);
        browser.click(By.xpath(dateDepartureLocator));
        browser.click(CONTINUE_BUTTON_LOCATOR);
        return this;
    }

    public BuyTicketPage selectTravelTime() {
        int i = 1;
        String travelTimeLocator = "//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form2:id2:tableEx1']/tbody/tr[" + i +"]/td[7]";
        while (!browser.read(By.xpath(travelTimeLocator)).equals("02:59")) {
            i++;
            travelTimeLocator = "//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form2:id2:tableEx1']/tbody/tr[" + i +"]/td[7]";
        }
        String trainLocator = "//*[@id='viewns_7_48QFVAUK6HA180IQAQVJU80004_:form2:id2:tableEx1']/tbody/tr[" + i +"]/td[1]/span/input";
        browser.click(By.xpath(trainLocator));
        return this;
    }

    public BuyTicketPage selectTrainCar() {
        browser.click(TRAIN_CAR_LOCATOR);
        return this;
    }

    public BuyTicketPage sendDataByPassenger(Passenger passenger) {
        browser.type(LASTNAME_INPUT_LOCATOR, passenger.getLastname());
        browser.type(NAME_INPUT_LOCATOR, passenger.getName());
        browser.type(PASSPORT_INPUT_LOCATOR, passenger.getPassport());
        return this;
    }

    public BuyTicketPage selectPlaceInTrainCar() {
        browser.click(PLACE_LOCATOR);
        return this;
    }

    public BuyTicketPage continueBuyTicket() {
        browser.click(YES_CONDITION_CHECKBOX);
        browser.click(CONTINUE_BUY_BUTTON);
        browser.click(BUY_TICKET_BUTTON);
        return this;
    }

    public PaymentPage clickPaymentPage() {
        browser.click(YES_BUTTON);
        return new PaymentPage();
    }
}
