package pages;

import org.openqa.selenium.By;


public class PaymentPage extends AbstractPage {

    private static final By NUMBER_CARD_LOCATOR = By.xpath("//*[@id='CardNumberFieldVisa']/span/b");


    public boolean isDisplayedPaymentPage() {
        return browser.read(NUMBER_CARD_LOCATOR).equals("НОМЕР КАРТЫ");
    }
}
