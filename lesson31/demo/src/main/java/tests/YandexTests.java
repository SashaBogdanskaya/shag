package tests;

import bo.Letter;
import builder.LetterBuilder;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.InboxPage;
import pages.LoginPage;
import wd.WebDriverWrapper;

/**
 * Created by Account on 05.04.2017.
 */
public class YandexTests {

    @BeforeSuite
    public void initBrowser(){
        WebDriverWrapper.init();
    }

    @Test(description = "Yandex login test")
    public void yandexLoginTest(){
        InboxPage inboxPage = new LoginPage().open().inputLogin("shagtest").inputPassword("shagtest123").clickLoginButton();
        Assert.assertTrue(inboxPage.isLoginNameDisplayed());
    }


    @Test(description = "Send mail test", dependsOnMethods = "yandexLoginTest")
    public void sendMail(){
        Letter letter = LetterBuilder.createLetter();
        InboxPage inboxPage = new InboxPage().clickNewLetter().sendLetter(letter).openInbox();
        Assert.assertTrue(inboxPage.isLetterPresent(letter));
    }

    @AfterSuite(description = "Kill browser")
    public void killBrowser(){
        WebDriverWrapper.getInstance().close();
    }

    /*
    * Дописать isLetterPresent
    * Создать бизнес-объект Аккаунт, создать фабрику аккаунтов (умеет создавать верный и неверный аккаунты)
    * Написать негативный тест на логин (ассерт что сообщение высвечивается)    *
    * */
}
