import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Main on 03.04.2017.
 */
public class SeleniumTestLocal {
    private static WebDriver driver;

    public static void main(String[] args) throws MalformedURLException {

        System.setProperty("webdriver.chrome.driver", new File("src\\main\\resources\\chromedriver.exe").getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("http://www.ebay.com/");

        waitForElementPresent(By.xpath("//*[@id='gh-ac']"));// ожидаем поле ввода Покупки по категориям
        WebElement searchInput = driver.findElement(By.xpath("//*[@id='gh-ac']"));//
        searchInput.sendKeys("Iphone SE");// вводим в это поле Iphone SE

        waitForElementPresent(By.xpath("//*[@id='gh-btn']"));// кнопка Найти

        driver.findElement(By.xpath("//*[@id='gh-btn']")).click();//нажимаем кнопку Найти

        driver.close();


    }

    private static void waitForElementPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }


}
