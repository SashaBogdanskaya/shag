import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
public class Start {
    public static void main(String[] args) throws IOException {

        File rootDirectory = new File("src\\main\\resources");
        List<File> listOfFiles = (List<File>) FileUtils.listFiles(rootDirectory, null, true);

        //System.out.println(listOfFiles);

        int depth = 5;
        CountWords item = new CountWords();
        item.findFiles(rootDirectory,depth);
        System.out.println("");
        //item.splitToWords("src\\main\\resources\\books.txt");
    }
}
